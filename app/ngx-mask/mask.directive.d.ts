import { MaskService } from './mask.service';
import { IConfig } from './config';
export declare class MaskDirective {
    private _maskService;
    private _maskValue;
    constructor(_maskService: MaskService);
    maskExpression: string;
    specialCharacters: IConfig['specialCharacters'];
    patterns: IConfig['patterns'];
    dropSpecialCharacters: IConfig['dropSpecialCharacters'];
    clearIfNotMatch: IConfig['clearIfNotMatch'];
    onInput(e: KeyboardEvent): void;
    onBlur(): void;
}
