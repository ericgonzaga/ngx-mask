import { ElementRef, EventEmitter, Renderer2 } from '@angular/core';
import { IConfig } from './config';
import { ControlValueAccessor } from '@angular/forms';
import 'rxjs/add/operator/take';
export declare class MaskService implements ControlValueAccessor {
    private document;
    private _config;
    private _elementRef;
    private _renderer;
    dropSpecialCharacters: IConfig['dropSpecialCharacters'];
    clearIfNotMatch: IConfig['clearIfNotMatch'];
    maskExpression: string;
    maskSpecialCharacters: IConfig['specialCharacters'];
    maskAvailablePatterns: IConfig['patterns'];
    maskSetter$$: EventEmitter<string>;
    private _regExpForRemove;
    private _shift;
    private _formElement;
    onChange: (_: any) => void;
    onTouch: () => void;
    constructor(document: any, _config: IConfig, _elementRef: ElementRef, _renderer: Renderer2);
    applyMask(inputValue: string, maskExpression: string, position?: number, cb?: Function): string;
    applyValueChanges(position?: number, cb?: Function): void;
    clearIfNotMatchFn(): void;
    /** It writes the value in the input */
    writeValue(inputValue: string): Promise<void>;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
    /** It disables the input element */
    setDisabledState(isDisabled: boolean): void;
    private _removeMask(value);
    private _checkSymbolMask(inputSymbol, maskSymbol);
    private _formElementProperty;
}
