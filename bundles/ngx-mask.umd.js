(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('rxjs/add/operator/take'), require('@angular/forms')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common', 'rxjs/add/operator/take', '@angular/forms'], factory) :
	(factory((global['ngx-mask'] = {}),global.ng.core,global.ng.common,global.Rx.Observable.prototype,global.ng.forms));
}(this, (function (exports,core,common,take,forms) { 'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0
THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.
See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */






function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}
function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}


function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 */
var config = new core.InjectionToken('config');
var NEW_CONFIG = new core.InjectionToken('NEW_CONFIG');
var INITIAL_CONFIG = new core.InjectionToken('INITIAL_CONFIG');
var initialConfig = {
    clearIfNotMatch: false,
    dropSpecialCharacters: true,
    specialCharacters: ['/', '(', ')', '.', ':', '-', ' ', '+'],
    patterns: {
        '0': {
            pattern: new RegExp('\\d'),
        },
        '9': {
            pattern: new RegExp('\\d'),
            optional: true
        },
        'A': {
            pattern: new RegExp('\[a-zA-Z0-9\]')
        },
        'S': {
            pattern: new RegExp('\[a-zA-Z\]')
        }
    }
};
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MaskService = /** @class */ (function () {
    /**
     * @param {?} document
     * @param {?} _config
     * @param {?} _elementRef
     * @param {?} _renderer
     */
    function MaskService(
    // tslint:disable-next-line
    document, _config, _elementRef, _renderer) {
        this.document = document;
        this._config = _config;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.maskExpression = '';
        this.maskSetter$$ = new core.EventEmitter();
        this.onChange = function (_) { };
        this.onTouch = function () { };
        this._shift = new Set();
        this.clearIfNotMatch = this._config.clearIfNotMatch;
        this.dropSpecialCharacters = this._config.dropSpecialCharacters;
        this.maskSpecialCharacters = /** @type {?} */ ((this._config)).specialCharacters;
        this.maskAvailablePatterns = this._config.patterns;
        this._regExpForRemove = new RegExp(this.maskSpecialCharacters
            .map(function (item) { return "\\" + item; })
            .join('|'), 'gi');
        this._formElement = this._elementRef.nativeElement;
    }
    /**
     * @param {?} inputValue
     * @param {?} maskExpression
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    MaskService.prototype.applyMask = function (inputValue, maskExpression, position, cb) {
        if (position === void 0) { position = 0; }
        if (cb === void 0) { cb = function () { }; }
        if (inputValue === undefined || inputValue === null) {
            return '';
        }
        var /** @type {?} */ cursor = 0;
        var /** @type {?} */ result = '';
        var /** @type {?} */ inputArray = inputValue.toString()
            .split('');
        // tslint:disable-next-line
        for (var /** @type {?} */ i = 0, /** @type {?} */ inputSymbol = inputArray[0]; i
            < inputArray.length; i++, inputSymbol = inputArray[i]) {
            if (result.length === maskExpression.length) {
                break;
            }
            if (this._checkSymbolMask(inputSymbol, maskExpression[cursor])) {
                result += inputSymbol;
                cursor++;
            }
            else if (this.maskSpecialCharacters.indexOf(maskExpression[cursor]) !== -1) {
                result += maskExpression[cursor];
                cursor++;
                this._shift.add(cursor);
                i--;
            }
            else if (this.maskSpecialCharacters.indexOf(inputSymbol) > -1
                && this.maskAvailablePatterns[maskExpression[cursor]]
                && this.maskAvailablePatterns[maskExpression[cursor]].optional) {
                cursor++;
                i--;
            }
        }
        if (result.length + 1 === maskExpression.length
            && this.maskSpecialCharacters.indexOf(maskExpression[maskExpression.length - 1]) !== -1) {
            result += maskExpression[maskExpression.length - 1];
        }
        var /** @type {?} */ shift = 1;
        var /** @type {?} */ newPosition = position + 1;
        while (this._shift.has(newPosition)) {
            shift++;
            newPosition++;
        }
        cb(this._shift.has(position) ? shift : 0);
        return result;
    };
    /**
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    MaskService.prototype.applyValueChanges = function (position, cb) {
        if (position === void 0) { position = 0; }
        if (cb === void 0) { cb = function () { }; }
        var /** @type {?} */ maskedInput = this.applyMask(this._formElement.value, this.maskExpression, position, cb);
        this._formElement.value = maskedInput;
        this.dropSpecialCharacters === true
            ? this.onChange(this._removeMask(maskedInput))
            : this.onChange(maskedInput);
        if (this._formElement === this.document.activeElement) {
            return;
        }
        this.clearIfNotMatchFn();
    };
    /**
     * @return {?}
     */
    MaskService.prototype.clearIfNotMatchFn = function () {
        if (this.clearIfNotMatch === true && this.maskExpression.length
            !== this._formElement.value.length) {
            this._formElementProperty = ['value', ''];
        }
    };
    /**
     * It writes the value in the input
     * @param {?} inputValue
     * @return {?}
     */
    MaskService.prototype.writeValue = function (inputValue) {
        return __awaiter(this, void 0, void 0, function () {
            var maskExpression, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (inputValue === undefined) {
                            return [2 /*return*/];
                        }
                        _a = this.maskExpression;
                        if (_a) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.maskSetter$$.take(1)
                                .toPromise()];
                    case 1:
                        _a = (_b.sent());
                        _b.label = 2;
                    case 2:
                        maskExpression = _a;
                        inputValue
                            ? this._formElementProperty = ['value', this.applyMask(inputValue, maskExpression)]
                            : this._formElementProperty = ['value', ''];
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    MaskService.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    MaskService.prototype.registerOnTouched = function (fn) {
        this.onTouch = fn;
    };
    /**
     * It disables the input element
     * @param {?} isDisabled
     * @return {?}
     */
    MaskService.prototype.setDisabledState = function (isDisabled) {
        isDisabled
            ? this._formElementProperty = ['disabled', 'true']
            : this._formElementProperty = ['disabled', 'false'];
    };
    /**
     * @param {?} value
     * @return {?}
     */
    MaskService.prototype._removeMask = function (value) {
        return value
            ? value.replace(this._regExpForRemove, '')
            : value;
    };
    /**
     * @param {?} inputSymbol
     * @param {?} maskSymbol
     * @return {?}
     */
    MaskService.prototype._checkSymbolMask = function (inputSymbol, maskSymbol) {
        return inputSymbol
            === maskSymbol
            || this.maskAvailablePatterns[maskSymbol] && this.maskAvailablePatterns[maskSymbol].pattern
                && this.maskAvailablePatterns[maskSymbol].pattern.test(inputSymbol);
    };
    Object.defineProperty(MaskService.prototype, "_formElementProperty", {
        /**
         * @param {?} __0
         * @return {?}
         */
        set: function (_a) {
            var _b = __read(_a, 2), name = _b[0], value = _b[1];
            this._renderer.setProperty(this._formElement, name, value);
        },
        enumerable: true,
        configurable: true
    });
    return MaskService;
}());
MaskService.decorators = [
    { type: core.Injectable },
];
/** @nocollapse */
MaskService.ctorParameters = function () { return [
    { type: undefined, decorators: [{ type: core.Inject, args: [common.DOCUMENT,] },] },
    { type: undefined, decorators: [{ type: core.Inject, args: [config,] },] },
    { type: core.ElementRef, },
    { type: core.Renderer2, },
]; };
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MaskDirective = /** @class */ (function () {
    /**
     * @param {?} _maskService
     */
    function MaskDirective(_maskService) {
        this._maskService = _maskService;
    }
    Object.defineProperty(MaskDirective.prototype, "maskExpression", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            this._maskValue = value || '';
            if (!this._maskValue) {
                return;
            }
            this._maskService.maskExpression = this._maskValue;
            this._maskService.maskSetter$$.emit(this._maskValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskDirective.prototype, "specialCharacters", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            if (!value || !Array.isArray(value) || Array.isArray(value) && !value.length) {
                return;
            }
            this._maskService.maskSpecialCharacters = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskDirective.prototype, "patterns", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            if (!value) {
                return;
            }
            this._maskService.maskAvailablePatterns = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskDirective.prototype, "dropSpecialCharacters", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            this._maskService.dropSpecialCharacters = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskDirective.prototype, "clearIfNotMatch", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            this._maskService.clearIfNotMatch = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} e
     * @return {?}
     */
    MaskDirective.prototype.onInput = function (e) {
        if (!this._maskValue) {
            return;
        }
        var /** @type {?} */ el = ((e.target));
        var /** @type {?} */ position = el.selectionStart;
        var /** @type {?} */ caretShift = 0;
        this._maskService.applyValueChanges(position, function (shift) { return caretShift = shift; });
        el.selectionStart = el.selectionEnd = position + (
        // tslint:disable-next-line
        ((e)).inputType === 'deleteContentBackward'
            ? 0
            : caretShift);
    };
    /**
     * @return {?}
     */
    MaskDirective.prototype.onBlur = function () {
        this._maskService.clearIfNotMatchFn();
        this._maskService.onTouch();
    };
    return MaskDirective;
}());
MaskDirective.decorators = [
    { type: core.Directive, args: [{
                selector: '[mask]',
                providers: [
                    {
                        provide: forms.NG_VALUE_ACCESSOR,
                        useExisting: MaskService,
                        multi: true
                    },
                    MaskService
                ],
            },] },
];
/** @nocollapse */
MaskDirective.ctorParameters = function () { return [
    { type: MaskService, },
]; };
MaskDirective.propDecorators = {
    "maskExpression": [{ type: core.Input, args: ['mask',] },],
    "specialCharacters": [{ type: core.Input },],
    "patterns": [{ type: core.Input },],
    "dropSpecialCharacters": [{ type: core.Input },],
    "clearIfNotMatch": [{ type: core.Input },],
    "onInput": [{ type: core.HostListener, args: ['input', ['$event'],] },],
    "onBlur": [{ type: core.HostListener, args: ['blur',] },],
};
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var NgxMaskModule = /** @class */ (function () {
    function NgxMaskModule() {
    }
    /**
     * @param {?=} configValue
     * @return {?}
     */
    NgxMaskModule.forRoot = function (configValue) {
        return {
            ngModule: NgxMaskModule,
            providers: [
                {
                    provide: NEW_CONFIG,
                    useValue: configValue
                },
                {
                    provide: INITIAL_CONFIG,
                    useValue: initialConfig
                },
                {
                    provide: config,
                    useFactory: _configFactory,
                    deps: [INITIAL_CONFIG, NEW_CONFIG]
                },
            ]
        };
    };
    return NgxMaskModule;
}());
NgxMaskModule.decorators = [
    { type: core.NgModule, args: [{
                imports: [common.CommonModule],
                exports: [MaskDirective],
                declarations: [MaskDirective]
            },] },
];
/** @nocollapse */
NgxMaskModule.ctorParameters = function () { return []; };
/**
 * \@internal
 * @param {?} initConfig
 * @param {?} configValue
 * @return {?}
 */
function _configFactory(initConfig, configValue) {
    return (typeof configValue === 'function') ? configValue() : Object.assign({}, initConfig, configValue);
}

exports.config = config;
exports.NEW_CONFIG = NEW_CONFIG;
exports.INITIAL_CONFIG = INITIAL_CONFIG;
exports.initialConfig = initialConfig;
exports.MaskDirective = MaskDirective;
exports.MaskService = MaskService;
exports.NgxMaskModule = NgxMaskModule;
exports._configFactory = _configFactory;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ngx-mask.umd.js.map
