import { InjectionToken, ElementRef, EventEmitter, Inject, Injectable, Renderer2, Directive, HostListener, Input, NgModule } from '@angular/core';
import { __awaiter } from 'tslib';
import { DOCUMENT, CommonModule } from '@angular/common';
import 'rxjs/add/operator/take';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 */

const config = new InjectionToken('config');
const NEW_CONFIG = new InjectionToken('NEW_CONFIG');
const INITIAL_CONFIG = new InjectionToken('INITIAL_CONFIG');
const initialConfig = {
    clearIfNotMatch: false,
    dropSpecialCharacters: true,
    specialCharacters: ['/', '(', ')', '.', ':', '-', ' ', '+'],
    patterns: {
        '0': {
            pattern: new RegExp('\\d'),
        },
        '9': {
            pattern: new RegExp('\\d'),
            optional: true
        },
        'A': {
            pattern: new RegExp('\[a-zA-Z0-9\]')
        },
        'S': {
            pattern: new RegExp('\[a-zA-Z\]')
        }
    }
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MaskService {
    /**
     * @param {?} document
     * @param {?} _config
     * @param {?} _elementRef
     * @param {?} _renderer
     */
    constructor(
            // tslint:disable-next-line
            document, _config, _elementRef, _renderer) {
            this.document = document;
            this._config = _config;
            this._elementRef = _elementRef;
            this._renderer = _renderer;
            this.maskExpression = '';
            this.maskSetter$$ = new EventEmitter();
            this.onChange = (_) => {};
            this.onTouch = () => {};
            this._shift = new Set();
            this.clearIfNotMatch = this._config.clearIfNotMatch;
            this.dropSpecialCharacters = this._config.dropSpecialCharacters;
            this.maskSpecialCharacters = /** @type {?} */ ((this._config)).specialCharacters;
            this.maskAvailablePatterns = this._config.patterns;
            this._regExpForRemove = new RegExp(this.maskSpecialCharacters
                .map((item) => `\\${item}`)
                .join('|'), 'gi');
            this._formElement = this._elementRef.nativeElement;
        }
        /**
         * @param {?} inputValue
         * @param {?} maskExpression
         * @param {?=} position
         * @param {?=} cb
         * @return {?}
         */
    applyMask(inputValue, maskExpression, position = 0, cb = () => {}) {
            if (inputValue === undefined || inputValue === null) {
                return '';
            }
            if (maskExpression === undefined || maskExpression === null || maskExpression === '') {
                return inputValue;
            }
            let /** @type {?} */ cursor = 0;
            let /** @type {?} */ result = '';
            const /** @type {?} */ inputArray = inputValue.toString()
                .split('');
            // tslint:disable-next-line
            for (let /** @type {?} */ i = 0, /** @type {?} */ inputSymbol = inputArray[0]; i <
                inputArray.length; i++, inputSymbol = inputArray[i]) {
                if (result.length === maskExpression.length) {
                    break;
                }
                if (this._checkSymbolMask(inputSymbol, maskExpression[cursor])) {
                    result += inputSymbol;
                    cursor++;
                } else if (this.maskSpecialCharacters.indexOf(maskExpression[cursor]) !== -1) {
                    result += maskExpression[cursor];
                    cursor++;
                    this._shift.add(cursor);
                    i--;
                } else if (this.maskSpecialCharacters.indexOf(inputSymbol) > -1 &&
                    this.maskAvailablePatterns[maskExpression[cursor]] &&
                    this.maskAvailablePatterns[maskExpression[cursor]].optional) {
                    cursor++;
                    i--;
                }
            }
            if (result.length + 1 === maskExpression.length &&
                this.maskSpecialCharacters.indexOf(maskExpression[maskExpression.length - 1]) !== -1) {
                result += maskExpression[maskExpression.length - 1];
            }
            let /** @type {?} */ shift = 1;
            let /** @type {?} */ newPosition = position + 1;
            while (this._shift.has(newPosition)) {
                shift++;
                newPosition++;
            }
            cb(this._shift.has(position) ? shift : 0);
            return result;
        }
        /**
         * @param {?=} position
         * @param {?=} cb
         * @return {?}
         */
    applyValueChanges(position = 0, cb = () => {}) {
            const /** @type {?} */ maskedInput = this.applyMask(this._formElement.value, this.maskExpression, position, cb);
            this._formElement.value = maskedInput;
            this.dropSpecialCharacters === true ?
                this.onChange(this._removeMask(maskedInput)) :
                this.onChange(maskedInput);
            if (this._formElement === this.document.activeElement) {
                return;
            }
            this.clearIfNotMatchFn();
        }
        /**
         * @return {?}
         */
    clearIfNotMatchFn() {
            if (this.clearIfNotMatch === true && this.maskExpression.length !==
                this._formElement.value.length) {
                this._formElementProperty = ['value', ''];
            }
        }
        /**
         * It writes the value in the input
         * @param {?} inputValue
         * @return {?}
         */
    writeValue(inputValue) {
            return __awaiter(this, void 0, void 0, function*() {
                if (inputValue === undefined) {
                    return;
                }
                const /** @type {?} */ maskExpression = this.maskExpression || (yield this.maskSetter$$.take(1)
                    .toPromise());
                inputValue
                    ?
                    this._formElementProperty = ['value', this.applyMask(inputValue, maskExpression)] :
                    this._formElementProperty = ['value', ''];
            });
        }
        /**
         * @param {?} fn
         * @return {?}
         */
    registerOnChange(fn) {
            this.onChange = fn;
        }
        /**
         * @param {?} fn
         * @return {?}
         */
    registerOnTouched(fn) {
            this.onTouch = fn;
        }
        /**
         * It disables the input element
         * @param {?} isDisabled
         * @return {?}
         */
    setDisabledState(isDisabled) {
            isDisabled
                ?
                this._formElementProperty = ['disabled', 'true'] :
                this._formElementProperty = ['disabled', 'false'];
        }
        /**
         * @param {?} value
         * @return {?}
         */
    _removeMask(value) {
            return value ?
                value.replace(this._regExpForRemove, '') :
                value;
        }
        /**
         * @param {?} inputSymbol
         * @param {?} maskSymbol
         * @return {?}
         */
    _checkSymbolMask(inputSymbol, maskSymbol) {
            return inputSymbol ===
                maskSymbol ||
                this.maskAvailablePatterns[maskSymbol] && this.maskAvailablePatterns[maskSymbol].pattern &&
                this.maskAvailablePatterns[maskSymbol].pattern.test(inputSymbol);
        }
        /**
         * @param {?} __0
         * @return {?}
         */
    set _formElementProperty([name, value]) {
        this._renderer.setProperty(this._formElement, name, value);
    }
}
MaskService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MaskService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT, ] }, ] },
    { type: undefined, decorators: [{ type: Inject, args: [config, ] }, ] },
    { type: ElementRef, },
    { type: Renderer2, },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class MaskDirective {
    /**
     * @param {?} _maskService
     */
    constructor(_maskService) {
            this._maskService = _maskService;
        }
        /**
         * @param {?} value
         * @return {?}
         */
    set maskExpression(value) {
            this._maskValue = value || '';
            if (!this._maskValue) {
                return;
            }
            this._maskService.maskExpression = this._maskValue;
            this._maskService.maskSetter$$.emit(this._maskValue);
        }
        /**
         * @param {?} value
         * @return {?}
         */
    set specialCharacters(value) {
            if (!value || !Array.isArray(value) || Array.isArray(value) && !value.length) {
                return;
            }
            this._maskService.maskSpecialCharacters = value;
            this._maskService._regExpForRemove = new RegExp(value.map(function(item) { return "\\" + item; }).join('|'), 'gi');
        }
        /**
         * @param {?} value
         * @return {?}
         */
    set patterns(value) {
            if (!value) {
                return;
            }
            this._maskService.maskAvailablePatterns = value;
        }
        /**
         * @param {?} value
         * @return {?}
         */
    set dropSpecialCharacters(value) {
            this._maskService.dropSpecialCharacters = value;
        }
        /**
         * @param {?} value
         * @return {?}
         */
    set clearIfNotMatch(value) {
            this._maskService.clearIfNotMatch = value;
        }
        /**
         * @param {?} e
         * @return {?}
         */
    onInput(e) {
            const /** @type {?} */ el = ( /** @type {?} */ (e.target));
            const /** @type {?} */ position = el.selectionStart;
            let /** @type {?} */ caretShift = 0;
            this._maskService.applyValueChanges(position, (shift) => caretShift = shift);
            el.selectionStart = el.selectionEnd = position + (
                // tslint:disable-next-line
                ( /** @type {?} */ (e)).inputType === 'deleteContentBackward' ?
                0 :
                caretShift);
        }
        /**
         * @return {?}
         */
    onBlur() {
        this._maskService.clearIfNotMatchFn();
        this._maskService.onTouch();
    }
}
MaskDirective.decorators = [{
    type: Directive,
    args: [{
        selector: '[mask]',
        providers: [{
                provide: NG_VALUE_ACCESSOR,
                useExisting: MaskService,
                multi: true
            },
            MaskService
        ],
    }, ]
}, ];
/** @nocollapse */
MaskDirective.ctorParameters = () => [
    { type: MaskService, },
];
MaskDirective.propDecorators = {
    "maskExpression": [{ type: Input, args: ['mask', ] }, ],
    "specialCharacters": [{ type: Input }, ],
    "patterns": [{ type: Input }, ],
    "dropSpecialCharacters": [{ type: Input }, ],
    "clearIfNotMatch": [{ type: Input }, ],
    "onInput": [{ type: HostListener, args: ['input', ['$event'], ] }, ],
    "onBlur": [{ type: HostListener, args: ['blur', ] }, ],
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
class NgxMaskModule {
    /**
     * @param {?=} configValue
     * @return {?}
     */
    static forRoot(configValue) {
        return {
            ngModule: NgxMaskModule,
            providers: [{
                    provide: NEW_CONFIG,
                    useValue: configValue
                },
                {
                    provide: INITIAL_CONFIG,
                    useValue: initialConfig
                },
                {
                    provide: config,
                    useFactory: _configFactory,
                    deps: [INITIAL_CONFIG, NEW_CONFIG]
                },
            ]
        };
    }
}
NgxMaskModule.decorators = [{
    type: NgModule,
    args: [{
        imports: [CommonModule],
        exports: [MaskDirective],
        declarations: [MaskDirective]
    }, ]
}, ];
/** @nocollapse */
NgxMaskModule.ctorParameters = () => [];
/**
 * \@internal
 * @param {?} initConfig
 * @param {?} configValue
 * @return {?}
 */
function _configFactory(initConfig, configValue) {
    return (typeof configValue === 'function') ? configValue() : Object.assign({}, initConfig, configValue);
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */

export { config, NEW_CONFIG, INITIAL_CONFIG, initialConfig, MaskDirective, MaskService, NgxMaskModule, _configFactory };
//# sourceMappingURL=ngx-mask.js.map