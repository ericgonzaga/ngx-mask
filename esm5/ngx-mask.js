import * as tslib_1 from "tslib";
import { InjectionToken, ElementRef, EventEmitter, Inject, Injectable, Renderer2, Directive, HostListener, Input, NgModule } from '@angular/core';
import { __awaiter } from 'tslib';
import { DOCUMENT, CommonModule } from '@angular/common';
import 'rxjs/add/operator/take';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @record
 */
var config = new InjectionToken('config');
var NEW_CONFIG = new InjectionToken('NEW_CONFIG');
var INITIAL_CONFIG = new InjectionToken('INITIAL_CONFIG');
var initialConfig = {
    clearIfNotMatch: false,
    dropSpecialCharacters: true,
    specialCharacters: ['/', '(', ')', '.', ':', '-', ' ', '+'],
    patterns: {
        '0': {
            pattern: new RegExp('\\d'),
        },
        '9': {
            pattern: new RegExp('\\d'),
            optional: true
        },
        'A': {
            pattern: new RegExp('\[a-zA-Z0-9\]')
        },
        'S': {
            pattern: new RegExp('\[a-zA-Z\]')
        }
    }
};
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MaskService = /** @class */ (function() {
    /**
     * @param {?} document
     * @param {?} _config
     * @param {?} _elementRef
     * @param {?} _renderer
     */
    function MaskService(
        document, _config, _elementRef, _renderer) {
        this.document = document;
        this._config = _config;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.maskExpression = '';
        this.maskSetter$$ = new EventEmitter();
        this.onChange = function(_) {};
        this.onTouch = function() {};
        this._shift = new Set();
        this.clearIfNotMatch = this._config.clearIfNotMatch;
        this.dropSpecialCharacters = this._config.dropSpecialCharacters;
        this.maskSpecialCharacters = /** @type {?} */ ((this._config)).specialCharacters;
        this.maskAvailablePatterns = this._config.patterns;
        this._regExpForRemove = new RegExp(this.maskSpecialCharacters.map(function(item) { return "\\" + item; }).join('|'), 'gi');
        this._formElement = this._elementRef.nativeElement;
    }
    /**
     * @param {?} inputValue
     * @param {?} maskExpression
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    MaskService.prototype.applyMask = function(inputValue, maskExpression, position, cb) {

        if (position === void 0) { position = 0; }
        if (cb === void 0) { cb = function() {}; }
        if (inputValue === undefined || inputValue === null) {
            return '';
        }
        if (maskExpression === undefined || maskExpression === null || maskExpression === '') {
            return inputValue;
        }
        var /** @type {?} */ cursor = 0;
        var /** @type {?} */ result = '';
        var /** @type {?} */ inputArray = inputValue.toString()
            .split('');
        // tslint:disable-next-line
        for (var /** @type {?} */ i = 0, /** @type {?} */ inputSymbol = inputArray[0]; i <
            inputArray.length; i++, inputSymbol = inputArray[i]) {
            if (result.length === maskExpression.length) {
                break;
            }
            if (this._checkSymbolMask(inputSymbol, maskExpression[cursor])) {
                result += inputSymbol;
                cursor++;
            } else if (this.maskSpecialCharacters.indexOf(maskExpression[cursor]) !== -1) {
                result += maskExpression[cursor];
                cursor++;
                this._shift.add(cursor);
                i--;
            } else if (this.maskSpecialCharacters.indexOf(inputSymbol) > -1 &&
                this.maskAvailablePatterns[maskExpression[cursor]] &&
                this.maskAvailablePatterns[maskExpression[cursor]].optional) {
                cursor++;
                i--;
            }
        }
        if (result.length + 1 === maskExpression.length &&
            this.maskSpecialCharacters.indexOf(maskExpression[maskExpression.length - 1]) !== -1) {
            result += maskExpression[maskExpression.length - 1];
        }
        var /** @type {?} */ shift = 1;
        var /** @type {?} */ newPosition = position + 1;
        while (this._shift.has(newPosition)) {
            shift++;
            newPosition++;
        }
        cb(this._shift.has(position) ? shift : 0);
        return result;
    };
    /**
     * @param {?=} position
     * @param {?=} cb
     * @return {?}
     */
    MaskService.prototype.applyValueChanges = function(position, cb) {
        if (position === void 0) { position = 0; }
        if (cb === void 0) { cb = function() {}; }
        var /** @type {?} */ maskedInput = this.applyMask(this._formElement.value, this.maskExpression, position, cb);
        this._formElement.value = maskedInput;
        this.dropSpecialCharacters === true ?
            this.onChange(this._removeMask(maskedInput)) :
            this.onChange(maskedInput);
        if (this._formElement === this.document.activeElement) {
            return;
        }
        this.clearIfNotMatchFn();
    };
    /**
     * @return {?}
     */
    MaskService.prototype.clearIfNotMatchFn = function() {
        if (this.clearIfNotMatch === true && this.maskExpression.length !== this._formElement.value.length) {
            this._formElementProperty = ['value', ''];
        }
    };
    /**
     * It writes the value in the input
     * @param {?} inputValue
     * @return {?}
     */
    MaskService.prototype.writeValue = function(inputValue) {
        return __awaiter(this, void 0, void 0, function() {
            var maskExpression, _a;
            return tslib_1.__generator(this, function(_b) {
                switch (_b.label) {
                    case 0:
                        if (inputValue === undefined) {
                            return [2 /*return*/ ];
                        }
                        _a = this.maskExpression;
                        if (_a) return [3 /*break*/ , 2];
                        return [4 /*yield*/ , this.maskSetter$$.take(1)
                            .toPromise()
                        ];
                    case 1:
                        _a = (_b.sent());
                        _b.label = 2;
                    case 2:
                        maskExpression = _a;
                        inputValue ?
                            this._formElementProperty = ['value', this.applyMask(inputValue, maskExpression)] :
                            this._formElementProperty = ['value', ''];
                        return [2 /*return*/ ];
                }
            });
        });
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    MaskService.prototype.registerOnChange = function(fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    MaskService.prototype.registerOnTouched = function(fn) {
        this.onTouch = fn;
    };
    /**
     * It disables the input element
     * @param {?} isDisabled
     * @return {?}
     */
    MaskService.prototype.setDisabledState = function(isDisabled) {
        isDisabled
            ?
            this._formElementProperty = ['disabled', 'true'] :
            this._formElementProperty = ['disabled', 'false'];
    };
    /**
     * @param {?} value
     * @return {?}
     */
    MaskService.prototype._removeMask = function(value) {
        return value ?
            value.replace(this._regExpForRemove, '') :
            value;
    };
    /**
     * @param {?} inputSymbol
     * @param {?} maskSymbol
     * @return {?}
     */
    MaskService.prototype._checkSymbolMask = function(inputSymbol, maskSymbol) {
        return inputSymbol ===
            maskSymbol ||
            this.maskAvailablePatterns[maskSymbol] && this.maskAvailablePatterns[maskSymbol].pattern &&
            this.maskAvailablePatterns[maskSymbol].pattern.test(inputSymbol);
    };
    Object.defineProperty(MaskService.prototype, "_formElementProperty", {
        /**
         * @param {?} __0
         * @return {?}
         */
        set: function(_a) {
            var _b = tslib_1.__read(_a, 2),
                name = _b[0],
                value = _b[1];
            this._renderer.setProperty(this._formElement, name, value);
        },
        enumerable: true,
        configurable: true
    });
    return MaskService;
}());
MaskService.decorators = [
    { type: Injectable },
];
/** @nocollapse */
MaskService.ctorParameters = function() {
    return [
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT, ] }, ] },
        { type: undefined, decorators: [{ type: Inject, args: [config, ] }, ] },
        { type: ElementRef, },
        { type: Renderer2, },
    ];
};
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MaskDirective = /** @class */ (function() {
    /**
     * @param {?} _maskService
     */
    function MaskDirective(_maskService) {
        this._maskService = _maskService;
    }
    Object.defineProperty(MaskDirective.prototype, "maskExpression", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function(value) {
            this._maskValue = value || '';
            if (!this._maskValue) {
                return;
            }
            this._maskService.maskExpression = this._maskValue;
            this._maskService.maskSetter$$.emit(this._maskValue);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskDirective.prototype, "specialCharacters", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function(value) {
            if (!value || !Array.isArray(value) || Array.isArray(value) && !value.length) {
                return;
            }
            this._maskService.maskSpecialCharacters = value;
            this._maskService._regExpForRemove = new RegExp(value.map(function(item) { return "\\" + item; }).join('|'), 'gi');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskDirective.prototype, "patterns", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function(value) {
            if (!value) {
                return;
            }
            this._maskService.maskAvailablePatterns = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskDirective.prototype, "dropSpecialCharacters", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function(value) {
            this._maskService.dropSpecialCharacters = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MaskDirective.prototype, "clearIfNotMatch", {
        /**
         * @param {?} value
         * @return {?}
         */
        set: function(value) {
            this._maskService.clearIfNotMatch = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} e
     * @return {?}
     */
    MaskDirective.prototype.onInput = function(e) {
        /*
        if (!this._maskValue) {           
            return;
        }*/
        var /** @type {?} */ el = ((e.target));
        var /** @type {?} */ position = el.selectionStart;
        var /** @type {?} */ caretShift = 0;
        this._maskService.applyValueChanges(position, function(shift) { return caretShift = shift; });
        el.selectionStart = el.selectionEnd = position + (
            // tslint:disable-next-line
            ((e)).inputType === 'deleteContentBackward' ?
            0 :
            caretShift);
    };
    /**
     * @return {?}
     */
    MaskDirective.prototype.onBlur = function() {
        this._maskService.clearIfNotMatchFn();
        this._maskService.onTouch();
    };
    return MaskDirective;
}());
MaskDirective.decorators = [{
    type: Directive,
    args: [{
        selector: '[mask]',
        providers: [{
                provide: NG_VALUE_ACCESSOR,
                useExisting: MaskService,
                multi: true
            },
            MaskService
        ],
    }, ]
}, ];
/** @nocollapse */
MaskDirective.ctorParameters = function() {
    return [
        { type: MaskService, },
    ];
};
MaskDirective.propDecorators = {
    "maskExpression": [{ type: Input, args: ['mask', ] }, ],
    "specialCharacters": [{ type: Input }, ],
    "patterns": [{ type: Input }, ],
    "dropSpecialCharacters": [{ type: Input }, ],
    "clearIfNotMatch": [{ type: Input }, ],
    "onInput": [{ type: HostListener, args: ['input', ['$event'], ] }, ],
    "onBlur": [{ type: HostListener, args: ['blur', ] }, ],
};
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var NgxMaskModule = /** @class */ (function() {
    function NgxMaskModule() {}
    /**
     * @param {?=} configValue
     * @return {?}
     */
    NgxMaskModule.forRoot = function(configValue) {
        return {
            ngModule: NgxMaskModule,
            providers: [{
                    provide: NEW_CONFIG,
                    useValue: configValue
                },
                {
                    provide: INITIAL_CONFIG,
                    useValue: initialConfig
                },
                {
                    provide: config,
                    useFactory: _configFactory,
                    deps: [INITIAL_CONFIG, NEW_CONFIG]
                },
            ]
        };
    };
    return NgxMaskModule;
}());
NgxMaskModule.decorators = [{
    type: NgModule,
    args: [{
        imports: [CommonModule],
        exports: [MaskDirective],
        declarations: [MaskDirective]
    }, ]
}, ];
/** @nocollapse */
NgxMaskModule.ctorParameters = function() { return []; };
/**
 * \@internal
 * @param {?} initConfig
 * @param {?} configValue
 * @return {?}
 */
function _configFactory(initConfig, configValue) {
    return (typeof configValue === 'function') ? configValue() : Object.assign({}, initConfig, configValue);
}
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Generated bundle index. Do not edit.
 */
export { config, NEW_CONFIG, INITIAL_CONFIG, initialConfig, MaskDirective, MaskService, NgxMaskModule, _configFactory };
//# sourceMappingURL=ngx-mask.js.map